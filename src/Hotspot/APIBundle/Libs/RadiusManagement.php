<?php

namespace Hotspot\APIBundle\Libs;


class RadiusManagement {

    private $hotspot;

    /**
     * RadiusManagement constructor.
     * @param $hotspot
     */
    public function __construct($hotspot) {
        $this->hotspot = $hotspot;
    }

    public function getIndexContent() {

    }

    public function getAllUsers() {
        return $this->hotspot;

    }

    public function getOneUser($user_id) {

    }

    public function addUser() {

    }

    public function deleteUser($user_id) {

    }


}