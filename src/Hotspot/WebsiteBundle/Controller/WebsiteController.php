<?php

namespace Hotspot\WebsiteBundle\Controller;

use Common\UserBundle\CommonUserBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Hotspot\APIBundle\Libs\RadiusManagement as API;

class WebsiteController extends Controller {

    /**
     * @Route(
     *      "/",
     *      name = "website_index"
     * )
     *
     * @Template()
     */
    public function indexAction() {
        $this->checkSessionValidity();

        $api = new API($this->getActualHotspot());
        $hotspots = $api->getAllUsers();

        return array(
            'hotspots' => $hotspots,
        );
    }

    /**
     * @Route(
     *      "/users/{page}",
     *      name = "website_users",
     *     defaults = {"page" = 1},
     *     requirements = {"page" = "\d+"}
     * )
     *
     * @Template()
     */
    public function usersAction() {
        return array();
    }

    /**
     * @Route(
     *      "/new-user",
     *      name = "website_new_user"
     * )
     *
     * @Template("HotspotWebsiteBundle:Website:new_user.html.twig")
     */
    public function newUserAction() {
        return array();
    }

    /**
     * @Route(
     *      "/settings",
     *      name = "website_settings"
     * )
     *
     * @Template()
     */
    public function settingsAction() {
        return array();
    }

    /**
     * @Route(
     *      "/profile",
     *      name = "website_profile"
     * )
     *
     * @Template()
     */
    public function profileAction() {
        return array();
    }

    private function checkSessionValidity() {
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page.');

        if (!isset($_SESSION['hotspot_id'])) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $hotspots = $user->getHotspots()->getValues();
            if (!empty($hotspots)) {
                $_SESSION['hotspot_id'] = $hotspots[0]->getID();
            }
        }
    }

    private function getActualHotspot() {
        return $this->getDoctrine()->getRepository('HotspotWebsiteBundle:Hotspot')->find($_SESSION['hotspot_id']);
    }
}
