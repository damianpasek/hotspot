<?php

namespace Hotspot\WebsiteBundle\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hotspot\WebsiteBundle\Entity\Hotspot;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HotspotFixtures extends  AbstractFixture implements  OrderedFixtureInterface, ContainerAwareInterface {

    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $hotspotArray = array(
            array(
                'hotspotDbName' => 'hotel_hutnik_radius',
                'hotspotName' => 'Hotel Hutnik'
            ),
            array(
                'hotspotDbName' => 'gmina_nisko_radius',
                'hotspotName' => 'Gmina Nisko'
            )
        );

        foreach ($hotspotArray as $hotspot) {
            $tmpHotspot = new Hotspot();

            $tmpHotspot->setHotspotDbName($hotspot['hotspotDbName']);
            $tmpHotspot->setHotspotName($hotspot['hotspotName']);

            $manager->persist($tmpHotspot);
        }

        $manager->flush();
    }

    public function getOrder() {
        return 1;
    }


}