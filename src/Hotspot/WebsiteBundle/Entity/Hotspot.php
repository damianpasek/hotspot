<?php

namespace Hotspot\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Common\UserBundle\CommonUserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="Hotspot\WebsiteBundle\Repository\HotspotRepository")
 * @ORM\Table(name="hotspot")
 */
class Hotspot {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=30,
     *     name="hotspot_db_name",
     *     unique=true
     * )
     */
    private $hotspotDbName;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=30,
     *     name="hotspot_name",
     *     unique=true
     * )
     */
    private $hotspotName;

    /**
     * @ORM\Column(
     *     type="text",
     *     name="hotspot_description",
     *     nullable=true
     * )
     */
    private $hotspotDescription;

    /**
     * @ORM\Column(
     *     type="text",
     *     name="hotspot_login_page",
     *     nullable=true
     * )
     */
    private $hotspotLoginPage;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Common\UserBundle\Entity\User",
     *     mappedBy="hotspots"
     * )
     */
    private $users;

    public function getID() {
        return $this->id;
    }

    public function getHotspotDbName() {
        return $this->hotspotDbName;
    }

    public function setHotspotDbName($hotspotDbName) {
        $this->hotspotDbName = $hotspotDbName;
    }

    public function getHotspotName() {
        return $this->hotspotName;
    }

    public function setHotspotName($hotspotName) {
        $this->hotspotName = $hotspotName;
    }

    public function getHotspotDescription() {
        return $this->hotspotDescription;
    }

    public function setHotspotDescription($hotspotDescription) {
        $this->hotspotDescription = $hotspotDescription;
    }

    public function getHotspotLoginPage() {
        return $this->hotspotLoginPage;
    }

    public function setHotspotLoginPage($hotspotLoginPage) {
        $this->hotspotLoginPage = $hotspotLoginPage;
    }

    public function getUsers() {
        return $this->users;
    }

    public function setUsers($users) {
        $this->users = $users;
    }


}