<?php

namespace Hotspot\WebsiteBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Validator\Constraints as Assert;

class RadiusUser extends AbstractType {

    public function getName() {
        return 'radius_user';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add(
                'name', TextType::class, array(
                    'label' => 'Nazwa użytkownika',
                    'attr' => array(
                        'placeholder' => 'Nazwa użytkownika',
                    ),
                    'constraints' => array(
                        new Assert\NotBlank()
                    )
                )
            )->add(
                'password', PasswordType::class, array(
                    'label' => 'Hasło',
                    'attr' => array(
                        'placeholder' => 'Hasło',
                    ),
                    'constraints' => array(
                        new Assert\NotBlank()
                    )
                )
            )->add(
                'save', SubmitType::class, array(
                    'label' => 'Dodaj',
                )
            );
    }

}