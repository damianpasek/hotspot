<?php

namespace Common\UserBundle\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Common\UserBundle\Entity\User;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends  AbstractFixture implements  OrderedFixtureInterface, ContainerAwareInterface{


    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        $usersArray = array(
            array(
                'username' => 'damian',
                'email' => 'damianpasek@gmail.com',
                'password' => '123',
                'roles' => 'ROLE_SUPER_ADMIN'
            ),
            array(
                'username' => 'admin',
                'email' => 'admin@pasek.uk',
                'password' => '123',
                'roles' => 'ROLE_ADMIN'
            ),
            array(
                'username' => 'user',
                'email' => 'user@pasek.uk',
                'password' => '123',
                'roles' => 'ROLE_USER'
            )
        );

        $encoderFactory = $this->container->get('security.encoder_factory');

        foreach ($usersArray as $user) {
            $tmpUser = new User();

            $password = $encoderFactory->getEncoder($tmpUser)->encodePassword($user['password'], null);

            $tmpUser->setUsername($user['username'])
                    ->setEmail($user['email'])
                    ->setPassword($password)
                    ->setRoles($user['roles'])
                    ->setEnabled(true);

            $manager->persist($tmpUser);
        }

        $manager->flush();

    }

    public function getOrder() {
        return 0;
    }


}